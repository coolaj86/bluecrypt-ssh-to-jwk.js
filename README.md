# Bluecrypt SSH to JWK (for Browsers)

A minimal library to parse an SSH public key (`id_rsa.pub`)
and convert it into a public JWK using Vanilla JS.

Works for RSA and ECDSA public keys.

# Features

&lt; 150 lines of code | 1.0kb gzipped | 2.4kb minified | 3.7kb with comments

* [x] SSH Public Keys
* [x] RSA Public Keys
* [x] EC Public Keys
  * P-256 (prime256v1, secp256r1)
  * P-384 (secp384r1)
* [x] node.js version
  * [ssh-to-jwk.js](https://git.coolaj86.com/coolaj86/ssh-to-jwk.js)
* [x] on npm as [bluecrypt-jwk-to-ssh](https://www.npmjs.com/package/bluecrypt-ssh-to-jwk)

### Need SSH Private Keys?

SSH private keys (`id_rsa`) are just normal PEM files,
so you can use Eckles or Rasha, as mentioned above.

# Web Demo

<https://coolaj86.com/demos/ssh-to-jwk/>

<img border="1" src="https://i.imgur.com/g35NuLP.png" />

```bash
git clone https://git.coolaj86.com/coolaj86/bluecrypt-ssh-to-jwk.js
pushd bluecrypt-ssh-to-jwk.js/
```

```bash
open index.html
```

# Install

You can use it as a plain-old javascript library:

```html
<script src="https://git.coolaj86.com/coolaj86/bluecrypt-ssh-to-jwk.js/raw/branch/master/ssh-to-jwk.js"></script>
```

It's also on npm:

```bash
npm install bluecrypt-ssh-to-jwk
```

# Usage

Very simple:

```js
var pub = 'ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCE9Uli8bGnD4hOWdeo5KKQJ/P/vOazI4MgqJK54w37emP2JwOAOdMmXuwpxbKng3KZz27mz+nKWIlXJ3rzSGMo= root@localhost';

var ssh = SSH.parse(pub);

console.info(ssh.jwk);
```

# Other Tools in the Bluecrypt Suite

* [Bluecrypt JWK to SSH](https://git.coolaj86.com/coolaj86/bluecrypt-jwk-to-ssh.js) (RSA, EC, SSH)
* [Bluecrypt ASN.1 decoder](https://git.coolaj86.com/coolaj86/asn1-parser.js) (x509, RSA, EC, etc)
* [Bluecrypt ASN.1 builder](https://git.coolaj86.com/coolaj86/asn1-packer.js) (x509, RSA, EC, etc)

# Legal

[ssh-to-jwk.js](https://git.coolaj86.com/coolaj86/ssh-to-jwk.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)

Bluecrypt&trade; is owned by AJ ONeal
